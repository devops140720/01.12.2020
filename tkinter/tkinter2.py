from tkinter import *
import tkinter.font as font

def print_hello():
    print("Hello tkinter")

root = Tk()
root.title("Snake game")
lbl1 = Label(root, text='Hello world!')
lbl1.pack()

helloBtn = Button(root, text="Greet",
                  width=30,command=print_hello)
helloBtn['font'] = font.Font(size=20, weight='bold')
helloBtn.pack()

quit_button = Button(root, text="Quit",
                     fg="white",
                     bg = "blue",width=35,
                     font=font.Font(size=20),
                     command=root.quit)
quit_button.pack()

root.mainloop()

print("window closed")
from tkinter import *
import tkinter.font as font

class SnakeWindow:

    def print_hello(self):
        print("Hello tkinter")
        helloBtn = Button(self.root, text="Greet",
                          width=30,command=self.print_hello)
        helloBtn['font'] = font.Font(size=20, weight='bold')
        helloBtn.pack()

    def __init__(self, root):
        self.root = root
        self.root.title("Snake game")
        lbl1 = Label(self.root, text='Hello world!')
        lbl1.pack()

        helloBtn = Button(self.root, text="Greet",
                          width=30,command=self.print_hello)
        helloBtn['font'] = font.Font(size=20, weight='bold')
        helloBtn.pack()

        quit_button = Button(self.root, text="Quit",
                             fg="white",
                             bg = "blue",width=35,
                             font=font.Font(size=20),
                             command=root.quit)
        quit_button.pack()

root1 = Tk()
game_window = SnakeWindow(root1)
root1.mainloop()

root2 = Tk()
game_window2 = SnakeWindow(root2)
root2.mainloop()

print("window closed")
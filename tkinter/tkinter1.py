from tkinter import *

def print_hello():
    print("Hello tkinter")

root = Tk()
root.title("Snake game")
lbl1 = Label(root, text='Hello world!')
lbl1.pack()

helloBtn = Button(root, text="Greet", command=print_hello)
helloBtn.pack()

quit_button = Button(root, text="Quit", command=root.quit)
quit_button.pack()

root.mainloop()

print("window closed")